<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 27/08/2015
 * Time: 4:57 PM
 */


class RegisterController extends AppController
{

    var $name = 'Register';
    var $uses = array('Member');

    function beforeFilter()
    {
        parent::beforeFilter();
        $this->layout = '';
        $this->Auth->allow(array('register','savedata','check_name','check_email'));
    }
    function register()
    {
        $this->layout = 'default';

    }
    function savedata(){
        if (!empty($this->data)) {
            if ($this->Member->save($this->data)) {
                //$this->Session->setFlash('Registration completed.', true);
                $this->redirect(array('action' => 'register'));
            } else {
                debug("yes");
                //$this->Session->setFlash(__('Registration can\'t be done .', true));

            }
        }
    }
    function check_name() {
        $this->layout = 'ajax';
        $db = ConnectionManager::getDataSource("default"); // name of your database connection
        //$res = "SELECT * FROM registration  where username='$name'";
        $users = $this->Member->find('all', array('conditions'=>array('memberId'=>$this->RequestHandler->params['form']['name'])));
        $numUsers = sizeof($users);
        if($numUsers>0) {
            $check= "This Id Already Exists !";
        }
        else
            $check="Available";

        $this->set(compact('check'));
    }
    function check_email() {
        $this->layout = 'ajax';
        $db = ConnectionManager::getDataSource("default"); // name of your database connection
        //$res = "SELECT * FROM registration  where username='$name'";

        $users = $this->Member->find('all', array('conditions'=>array('email'=>$this->RequestHandler->params['form']['email'])));
        //debug($users);
        $numUsers = sizeof($users);
        if($numUsers>0) {
            $check= "Already Exists !";
        }
        else
            $check="Available";

        $this->set(compact('check'));
    }
}