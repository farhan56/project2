<div class="services form">
<?php echo $this->Form->create(null, array('url' => array('controller' => 'Specialist', 'action' => 'edit')));?>
	<fieldset>
		<legend><?php __('Edit Specialist'); ?></legend>
	<?php
		echo $this->Form->input('type',array('label'=>'Specialization Type'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>