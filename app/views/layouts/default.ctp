<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php //echo $this->Html->charset(); ?>
	<title>
		<?php //echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
        echo $this->Html->css('style');
		echo $this->Html->css('cake.generic');
        echo $this->Html->css('vas_style');
		//echo $scripts_for_layout;

	?>
<?php
	echo $this->Html->script('jquery');
    echo $this->Html->script('vas_js_functions.js');
	echo $this->Html->script('datepicker/date');
	echo $this->Html->script('datepicker/jquery.datePicker');
	echo $this->Html->script('datepicker/datePicker');
	echo $this->Html->css('datepicker/datePicker');
?>
</head>
<body id="main">
<div class="divwidth">
		<div style="padding: 20px;">
		</div>
	<div>
    <!-- startbody-->
    <div id="leftPnael">
    	<ul>

    		<?php echo '<li>'.$this->Html->link(__('Home',true), array('controller' => '', 'action' => '')).'</li>';?>
    		<?php echo '<li>'.$this->Html->link(__('User Profile',true), array('controller' => '', 'action' => '')).'</li>';?>


    		<?php echo '<li>'.$this->Html->link(__('Master Setup',true), array('controller' => '', 'action' => '')).'</li>';?>
			    <?php echo '<li>'.$this->Html->link(__('Notice ',true), array('controller' => '', 'action' => '')).'</li>';?>

			<?php echo '<li>'.$this->Html->link(__('Performance Reports',true), array('controller' => '', 'action' => '')).'</li>';?>



            <?php echo '<li>'.$this->Html->link(__('Message',true), array('controller' => '', 'action' => '')).'</li>';?>
    		<?php echo '<li>'.$this->Html->link(__('Settings',true), array('controller' => '', 'action' => '')).'</li>';?>
    		<?php echo '<li>'.$this->Html->link(__('Log Out',true), array('controller' => '', 'action' => '')).'</li>';?>

        </ul>

    </div>
    <!-- startbodyrightside-->
    <div id="right_Panel">
    	<div id="R_bodyinner">

			<div id="content">
				<?php echo $this->Session->flash(); ?>

				<?php echo $content_for_layout; ?>
			</div>

			<div class="clr"></div>
         </div>
    </div>
    <!-- endtbody-->
<div class="clr"></div>

</div>
     <div id="bottom">
     	<div class="clr"></div>
     </div>
	<div class="clr"></div>
</div>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>
