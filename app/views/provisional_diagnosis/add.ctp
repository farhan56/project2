<div class="services form">
<?php echo $this->Form->create(null, array('url' => array('controller' => 'ProvisionalDiagnosis', 'action' => 'add')));?>
	<fieldset>
		<legend><?php __('Add Provisional Diagnosis'); ?></legend>
	<?php
		echo $this->Form->input('disease_name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>