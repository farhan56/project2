<div class="services index">

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('Disease Name','disease_name');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($diagnosises as $diagnosis):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>

		<td><?php echo $diagnosis['ProvisionalDiagnosis']['disease_name']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $diagnosis['ProvisionalDiagnosis']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $diagnosis['ProvisionalDiagnosis']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $diagnosis['ProvisionalDiagnosis']['disease_name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>


	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>


<div class="actions">

	<ul>
		<li><?php echo $this->Html->link(__('Add Disease Name', true), array('controller' => 'ProvisionalDiagnosis', 'action' => 'add')); ?> </li>
	</ul>
</div>