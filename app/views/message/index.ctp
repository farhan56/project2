 <style>
   .box {margin-top:2px;  margin-left:60px; margin-right:-50px; margin-bottom:5px; top:0; left:0; right:0; bottom:0}
 </style>
 <h3>Inbox:</h3>
<div class="services index">

	<table cellpadding="0" cellspacing="0">
        <tr>

                <th><?php echo $this->Paginator->sort('Sender','to_user_id');?></th>
                <th><?php echo $this->Paginator->sort('','');?></th>
                <th><?php echo $this->Paginator->sort('Message','message');?></th>
                <th><?php echo $this->Paginator->sort('Sent On','sent_on');?></th>
                <th class="actions"><?php __('Actions');?></th>
        </tr>
	<?php
	$i = 0;
	foreach ($inbox as $user):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>

    <?php if($user['Message']['read']==0){?>
		<td>  <b><?php echo $user['User']['name']; ?>  </b>&nbsp;</td>
	     <td>
                           <div class="box" style="background:<?php if($user['Message']['category']=='3') echo '#FF0000';
                                           else if($user['Message']['category']=='2') echo '#FFFF00';
                                           else if($user['Message']['category']=='1') echo '#00FF00';
                                           else echo '';?>">
                                           .</div>
                        </td>
		<td><b><?php echo substr($user['Message']['message'],0,30).'....'; ?></b>&nbsp;</td>
		<td><b><?php echo $user['Message']['sent_on']; ?></b>&nbsp;</td>
		<?php } else {?>
		<td>  <?php echo $user['User']['name']; ?>  &nbsp;</td>
        	   <td>
                  <div class="box" style="background:<?php if($user['Message']['category']=='3') echo '#FF0000';
                                  else if($user['Message']['category']=='2') echo '#FFFF00';
                                  else if($user['Message']['category']=='1') echo '#00FF00';
                                  else echo '';?>">
                                  .</div>
               </td>
                    <td>

                    <?php echo substr($user['Message']['message'],0,30).'....'; ?>&nbsp;


                    </td>


        		<td><?php echo $user['Message']['sent_on']; ?>&nbsp;</td>
            <?php } ?>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view_inbox', $user['Message']['id'])); ?>

		</td>
	</tr>
<?php endforeach; ?>
	</table>


	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
&nbsp;&nbsp;&nbsp;
 <h3>Outbox:</h3>

<div class="services index">

	<table cellpadding="0" cellspacing="0">
        <tr>

                <th><?php echo $this->Paginator->sort('Sent to','from_user_id');?></th>
                <th><?php echo $this->Paginator->sort('','');?></th>
                <th><?php echo $this->Paginator->sort('Message','message');?></th>
                <th><?php echo $this->Paginator->sort('Sent On','sent_on');?></th>
                <th class="actions"><?php __('Actions');?></th>
        </tr>
	<?php
	$i = 0;
	foreach ($sent_messages as $user):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>

		<td><?php if($user['Message']['all']==0) {
        		echo $user['User_reciever']['name']; }
        		else echo "All";?>&nbsp;</td>
		<td>
            <div class="box" style="background:<?php if($user['Message']['category']=='3') echo '#FF0000';
                 else if($user['Message']['category']=='2') echo '#FFFF00';
                 else if($user['Message']['category']=='1') echo '#00FF00';
                 else echo '';?>">.</div>
            </td>
		<td><?php echo substr($user['Message']['message'],0,30).'....'; ?>&nbsp;</td>
		<td><?php echo $user['Message']['sent_on']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $user['Message']['id'])); ?>

		</td>
	</tr>
<?php endforeach; ?>
	</table>


	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>



<div class="actions">

	<ul>
		<li><?php echo $this->Html->link(__('Send New Message', true), array('controller' => 'Message', 'action' => 'add')); ?> </li>
	</ul>
</div>
