<script type="text/javascript">

    $(document).ready(function(){
        $('#CdragentDistrictId').change(function(){
            console.log($(this).val());
            $.ajax({
                url:'<?php echo $this->webroot; ?>HomeAgent/getLocation/',
                type:'POST',
                data:{district_id:$(this).val(),type:'thana'},
                success:function(data){
                    console.log(data);
                    $('#thanas').html(data);
                },
                error:function(data){
                    console.log(data.responseText);
                }

            });

        });




    });

</script>





<?php
    echo $this->Html->css('trc_style');
?>
<div class="services index">

    <table cellpadding="0" cellspacing="0">
        <tr>

            <th><?php echo $this->Paginator->sort('Phone Number','msisdn');?></th>
            <th><?php echo $this->Paginator->sort('Call Date','call_date');?></th>
            <th><?php echo $this->Paginator->sort('Agent','agent_id');?></th>

        </tr>
        <?php
        $i = 0;
        foreach ($caller_history as $user):
        $class = null;
        if ($i++ % 2 == 0) {
            $class = ' class="altrow"';
        }
        ?>
        <tr<?php echo $class;?>>

            <td><?php echo $user['Cdragent']['msisdn']; ?>&nbsp;</td>
            <td><?php echo $user['Cdragent']['call_date']; ?>&nbsp;</td>
            <td><?php echo $user['User']['name']; ?>&nbsp;</td>




        </tr>
        <?php endforeach; ?>
    </table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        |
        <?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
    </div>
</div>
<form action="/trc/HomeAgent/add" id="CdragentHomeagentForm" method="post" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="POST"></div>

		<legend><h2>Enter Call Detail Records</h2></legend>
  			<div class="simon-1">
               <table border="0">
                <tr>
                    <td style="width:110px!important;">
                        <input type="hidden" name="agent_id" value="4" id="CdragentAgentId">
                        <div class="input text required"><label for="CdragentMsisdn">Msisdn</label>
                    </td>

                    <td>
                        <input name="msisdn" type="text" maxlength="20" id="CdragentMsisdn" value=<?php echo $cli?>></div>
                    </td>
                </tr>

                </table>
                <div style="clear:both;"></div>
     	</div>



         <div class="simon-1 simon-2">
          <table>

            <tr>
                <td style="width:115px!important;">
                    <label for="CdragentCallType">Call Type</label>
                 </td>
                <td  style="width:150px!important;">

                	<input type="radio" name="call_type" id="CdragentCallTypeFalseCall" value="False Call" checked="checked">
                	<label for="CdragentCallTypeFalseCall">False Call</label>
                </td>

                <td style="width:150px!important;">

                 	<input type="radio" name="call_type" id="CdragentCallTypeGenuineCall" value="Genuine Call">
                 	<label for="CdragentCallTypeGenuineCall">Genuine Call</label>
                </td>
                <td>&nbsp;</td>
            </tr>
		</table>
         <div style="clear:both;"></div>
	</div>

		 <div class="simon-1 simon-2">
            <table border="0">
            	<tr>
                <td >
                 	 <label for="CdragentCampaignType">Campaign Type</label>
                 </td>

                    <td>

                         <input type="radio" name="campaign_type" id="CdragentCampaignTypeRegular" value="Regular" checked="checked">
                         <label for="CdragentCampaignTypeRegular">Regular</label>
                     </td>

                    <td>

                        <input type="radio" name="campaign_type" id="CdragentCampaignTypeSMSPromotional" value="SMS promotional">
                        <label for="CdragentCampaignTypeSMSPromotional">SMS promotional</label>
                    </td>

                   <td>

                        <input type="radio" name="campaign_type" id="CdragentCampaignTypeTVC" value="TVC">
                        <label for="CdragentCampaignTypeTVC">TVC</label>
                   </td>

            	   <td>

                        <input type="radio" name="campaign_type" id="CdragentCampaignTypeNewsPaper" value="News Paper">
                        <label for="CdragentCampaignTypeNewsPaper">News Paper</label>
                  </td>

            </tr>
        </table>
        <div style="clear:both;"></div>
	</div>
<div class="simon-1 simon-2">
            <table border="0">
            	<tr>
                <td >
                 	 <label for="CdragentServiceType">Service Type</label>
                 </td>

                    <td>

                         <input type="radio" name="service_type" id="CdragentServiceTypeDoctorInfo" value="doctor info" checked="checked">
                         <label for="CdragentServiceTypeDoctorInfo">Doctor Info</label>
                     </td>

                    <td>

                        <input type="radio" name="service_type" id="CdragentServiceTypeAmbulanceInfo" value="ambulance info">
                        <label for="CdragentServiceTypeAmbulanceInfo">Ambulance Info</label>
                    </td>

                   <td>

                        <input type="radio" name="service_type" id="CdragentServiceTypeHealthServiceComplain" value="health service_complain">
                        <label for="CdragentServiceTypeHealthServiceComplain">Health Service Complain</label>
                   </td>

            	   <td>

                        <input type="radio" name="service_type" id="CdragentServiceType16263Complain" value="16263 complain">
                        <label for="CdragentServiceType16263Complain">16263 complain</label>
                  </td>

            </tr>
        </table>
        <div style="clear:both;"></div>
	</div>


       <div class="simon-1 simon-2">
        <table>
        	<tr>
                <td style="width:113px!important;">
                    <label for="CdragentCallRegion">Call Region</label>
                  </td>
                  <td  style="width:150px!important;">

                        <input type="radio" name="call_region" id="CdragentCallRegionUrbanArea" value="Urban Area">
                        <label for="CdragentCallRegionUrbanArea">Urban Area</label>
                     </td>
                   <td style="width:155px!important;">

                        <input type="radio" name="call_region" id="CdragentCallRegionRuralArea" value="Rural Area" checked="checked">
                        <label for="CdragentCallRegionRuralArea">Rural Area</label>
                   </td>
                 <td>

                        <input type="radio" name="call_region" id="CdragentCallRegionNotDefined" value="Not Defined">
                        <label for="CdragentCallRegionNotDefined">Not Defined</label>
                 </td>
            	</tr>
            </table>
		 <div style="clear:both;"></div>
	</div>


      <div class="simon-3 simon-2">
        <table>
        	<tr>
            	<td  style="width:130px; text-align:left; font-size:12px; vertical-align:top">Location:</td>
            	<!--
            	<td>
					<label for="CdragentDistrictId">District</label>
                </td>
                -->
                <td style="width:135px!important; ">
                    <div>
                    	<select name="district_id" id="CdragentDistrictId">
                    	<option value="">District</option>
                            <?php foreach($districts as $id=>$name){?>
                                <option value=<?php echo $id ?>><?php echo $name ?></option>
                            <?php } ?>

                        </select>
                        </div>
					</td>
				<!--
                <td >
                	<label for="CdragentThanaId">Thana</label>
                 </td>
                 -->

                 <td style="width:175px!important;">

                  <div id="thanas">
                        <select name="thana_id" id="CdragentThanaId">
                            <option value="">Thana</option>
                                 <!--
                                <?php foreach($thanas as $id=>$name){?>
                                    <option value=<?php echo $id ?>><?php echo $name ?></option>
                                <?php } ?>
                                -->

                        </select>
                  </div>
                  </td>

                   <td>
                   <!--
                       <label for="CdragentUnionId">Union</label>

                       -->
                       <div id="unions">
                       <select name="union_id" id="CdragentUnionId">
                                               <option value="">Union</option>
                        </div>
                                               <!--
                        <?php foreach($unions as $id=>$name){?>
                            <option value=<?php echo $id ?>><?php echo $name ?></option>
                        <?php } ?>
                        -->

                      </select>
                    </td>
              </tr>
            </table>
            <div style="clear:both"></div>
        </div>
        <div class="simon-1 simon-2">
                     <table>
                     	<tr>
                             <td style="width:115px!important;">
                                <label for="CdragentGender">Gender</label>
                              </td>

                              <td style="width:150px!important;">

                                    <input type="radio" name="gender" id="CdragentGenderF" value="F">
                                    <label for="CdragentGenderF">Female</label>
                              </td>

                              <td>

                                    <input type="radio" name="gender" id="CdragentGenderM" value="M" checked="checked">
                                    <label for="CdragentGenderM">Male</label>
                               </td>

                        	</tr>

                        </table>
                        <div style="clear:both"></div>
                      </div>

          <div class="simon-1">
            <table>
            <tr>
            	<td>
            		<div class="input text required"><label for="CdragentAgeYear">Caller Age(Year)</label>
            		<input name="age_year" type="text" maxlength="4" id="CdragentAgeYear">
                </td>

                  <td>
                  	</div><div class="input text required"><label for="CdragentAgeMonth">Caller Age(Month)</label>
                  	<input name="age_month" type="text" maxlength="4" id="CdragentAgeMonth"
                   </td>
        	</tr>
       </table>
        <div style="clear:both"></div>
        </div>




            <div class="simon-1 simon-2">
             <table>
             	<tr>
                     <td style="width:115px!important;">
                        <label for="CdragentIsSatisfied">Is Satisfied</label>
                      </td>

                      <td style="width:150px!important;">

                            <input type="radio" name="is_satisfied" id="CdragentIsSatisfiedY" value="Y" checked="checked">
                            <label for="CdragentIsSatisfiedY">Yes</label>
                      </td>

                      <td>

                            <input type="radio" name="is_satisfied" id="CdragentIsSatisfiedN" value="N">
                            <label for="CdragentIsSatisfiedN">No</label>
                       </td>

                	</tr>

                </table>
                <div style="clear:both"></div>
              </div>

				<div class="simon-4">
                	 <table>
             			<tr>
                        	<td style="width:130px!important;">
                				<div class="input textarea required"><label for="CdragentNote">Note</label>
                             </td>
                             <td>
                 				<textarea name="note" options="" cols="30" rows="6" id="CdragentNote"></textarea>
                            </td>
                        </tr>
                      </table>
                  <div style="clear:both"></div>
              </div>



<div class="submit"><input type="submit" value="Submit"></div></form>