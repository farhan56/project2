<div class="services form">
<?php echo $this->Form->create(null, array('url' => array('controller' => 'UserRole', 'action' => 'Edit')));?>
	<fieldset>
		<legend><?php __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('created_on', array('style' => 'width:8%', 'default' => date('Y-m-d H:i:s')));
		echo $this->Form->input('updated_on', array('style' => 'width:8%', 'default' => date('Y-m-d H:i:s')));
        echo $this->Form->input('is_active', array('type' => 'select', 'options' => array('Y' => 'Yes', 'N' => 'No')));

	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>