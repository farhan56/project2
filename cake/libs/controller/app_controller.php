<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * This is a placeholder class.
 * Create the same file in app/app_controller.php
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.cake.libs.controller
 * @link http://book.cakephp.org/1.3/en/The-Manual/Developing-with-CakePHP/Controllers.html#the-app-controller
 */
class AppController extends Controller
{

    var $helpers = array('Form', 'Html', 'Session', 'Js', 'Javascript', 'Ajax','DatePicker');
    var $components = array('RequestHandler', 'Session', 'Auth');

    function beforeFilter()
    {
        parent::beforeFilter();

        $url = $_SERVER['REQUEST_URI']; //complete url
        if (preg_match('/home/', $url)){
            $this->Session->write('lastUrl', $url);
        }

        //$this->Auth->authenticate = $this->User;
        $this->RequestHandler->setContent('json', 'text/x-json');

        //$this->Auth->authorize = 'actions';
        //$this->Auth->authenticate = array('Form' => array('userModel' => 'User'));

        //$this->Auth->loginAction = array('controller' => 'Login', 'action' => 'login');
        //$this->Auth->loginRedirect = array('controller' => 'Home', 'action' => 'home','cli'=>'01737548866');
//        $this->Auth->logoutRedirect = array('controller' => 'Login', 'action' => 'login');

        //$this->Auth->userModel = 'User';
        //$this->Auth->fields = array('username' => 'login_id', 'password' => 'password');
        //$this->Auth->allow('*');


        $this->Auth->authError = 'You do not have permission to the page you just requested.Please Login';
        //$this->Auth->loginRedirect = array('controller' => 'DisplayFrame', 'action' => 'home');
    }
}
